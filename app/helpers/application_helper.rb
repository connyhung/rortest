module ApplicationHelper
  extend self

  def checksum(number)
    s1 = s2 = 0
    number.to_s.reverse.each_slice(2) do |odd, even|
      s1 += odd.to_i

      double = even.to_i * 2
      double -= 9 if double >= 10
      s2 += double
    end

    (s1 + s2) % 10
  end

  def check_digit(number)
    sum = checksum(number.to_i * 10)
    sum == 0 ? 0 : 10 - sum
  end

  # first answer
  def valid?(number)
    checksum(number) == 0
  end

  # second answer
  def append_check_digit(number)
    number.to_i * 10 + check_digit(number)
  end
end
