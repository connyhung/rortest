class Post < ApplicationRecord
  before_save :update_permalink

  def generate_permalink
    wish = title.parameterize
    taken = self.class.where(permalink: wish).exists?
    taken ? wish : "#{wish}-#{id}"
  end

  private

  def update_permalink
    if new_record? || title_changed?
      self.permalink = generate_permalink
    end
  end
end
