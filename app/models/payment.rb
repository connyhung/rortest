class Payment < ApplicationRecord
  belongs_to :service

  def self.with(query)
    query.assert_valid_keys(:line_item_id, :service_id)

    # first create payment if it does not exist
    # we want the code in block to work as fast as possible
    # because it locks entire table
    transaction(isolation: :serializable) do
      # we could do
      # create!(query) unless exists?(query)
      # but much better is
      connection.execute <<-SQL
        INSERT INTO payments (line_item_id, service_id)
        SELECT #{sanitize query[:line_item_id]}, #{sanitize query[:service_id]}
        WHERE NOT EXISTS (
          SELECT line_item_id FROM payments
          WHERE #{sanitize_sql_hash_for_conditions query}
        )
      SQL
    end

    # now we can lock a single record
    # and execute arbitrary code on it
    yield where(query).lock(true).first!

    # prevent method from accidental returning unsynchronized payment
    nil
  end
end
