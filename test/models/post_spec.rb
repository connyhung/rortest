require 'spec_helper'

describe Post do
  describe 'permalink' do
    it 'assigns' do
      post = Post.create! title: 'Hello world' # factory girl may be here
      expect(post.permalink).to eq('hello-world')
    end

    it 'changes' do
      post = Post.create! title: 'Hello world' # factory girl may be here
      post.update_attributes! title: 'Goodbye, world!!!'
      expect(post.permalink).to eq('goodbye-world')
    end

    it 'uniquifies' do
      post1 = Post.create! title: 'Hello world' # factory girl may be here
      post2 = Post.create! title: 'Hello world' # factory girl may be here
      expect(post2.permalink).to match('hello-world')
    end
  end
end
