class AddUniqueConstraintOnPayments < ActiveRecord::Migration[5.0]
  def change
    add_index :payments, [:line_item_id, :service_id], unique: true
  end
end
