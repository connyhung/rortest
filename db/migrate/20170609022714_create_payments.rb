class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.belongs_to :service
      t.string :email
      t.string :address
      t.integer :amount

      t.timestamps
    end
  end
end
